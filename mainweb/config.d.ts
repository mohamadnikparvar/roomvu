import { GlobalState } from './redux/store/reducer'

declare module 'react-redux' {
  export interface DefaultRootState extends GlobalState {}
}
