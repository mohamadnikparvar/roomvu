import React, { useEffect } from 'react';
import { StyledNavbar } from './styles';
import Toggle from 'react-toggle'
import WbSunnyTwoToneIcon from '@mui/icons-material/WbSunnyTwoTone';
import Brightness3TwoToneIcon from '@mui/icons-material/Brightness3TwoTone';
import { useDispatch, useSelector } from 'react-redux';
import { handledarkMode } from '@/redux/store/actions/actions';
import { GlobalState } from '@/redux/store/reducer';
import Link from 'next/link';

const Navbar = () => {
    const dispatch = useDispatch();

    // calling our state from the reduxer using useSelector hook of redux
    const { isdarkMode } = useSelector((state: GlobalState) => state.darkMode) as any;

    // function to be fired on onChange method to switch the mode
    const switchDarkMode = () => {
        isdarkMode
            ? dispatch(handledarkMode(false as unknown as string) as any)
            : dispatch(handledarkMode(true as unknown as string) as any);
    };
    useEffect(() => {
        document.body.style.backgroundColor = isdarkMode ? "#292c35" : "#fff";
        document.querySelectorAll('p').forEach(e => e.style.color = isdarkMode ? "hsla(0,0%,100%,0.88)" : "black");
        document.querySelectorAll('a').forEach(e => e.style.color = isdarkMode ? "#ffa7c4" : "#d23669");
        document.querySelectorAll('h1').forEach(e => e.style.color = isdarkMode ? "white" : "black");
    }, [isdarkMode]);
    return (
        <StyledNavbar >
            <header>
                <Link href='/'><h1>Overreacted</h1></Link>
                <div>
                    <Toggle

                        checked={isdarkMode}
                        onChange={switchDarkMode}
                        icons={{
                            checked: <WbSunnyTwoToneIcon />,
                            unchecked: <Brightness3TwoToneIcon />,
                        }}
                    />
                </div>
            </header>
        </StyledNavbar>
    );
};

export default Navbar;