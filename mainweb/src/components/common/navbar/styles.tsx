import { Box, styled } from '@mui/material'
import { BoxProps } from '@mui/system'

export const StyledNavbar = styled(Box)<BoxProps>(({ }) => {
    return {
        marginBottom: "42px",
        '& header': {
            display: 'flex',
            alignItems: "center",
            justifyContent: "space-between"
        },
        '& a': {
            textDecoration: 'none'
        },
        '& h1': {
            margin: "0",
            fontFamily: "Montserrat,sans-serif",
        },
    }
})
