import { Box, styled } from '@mui/material'
import { BoxProps } from '@mui/system'

export const StyledAboutMe = styled(Box)<BoxProps>(({ }) => {
    return {
        '& .aboutMeContainer': {
            display: "flex",
            alignItems: "center",
            marginBottom: " 3.5rem",
        },
        '& img': {
            borderRadius: "50%",
            marginRight: " 0.875rem",
        },
        '& .aboutUsDescription': {
            maxWidth: "300px",
            fontWeight: "400",
            fontSize: "18px",
            '& a': {
                color: "#d23669",
                textDecoration: "none",
                marginLeft: "5px",
            },

        },
    }
})
export const StyledArticleCards = styled(Box)<BoxProps>(({ }) => {
    return {

        '& .articleCardsTitle': {
            margin: "3.5rem 0 0.4375rem 0",
            '& a': {
                fontFamily: "system-ui",
                color: "#d23669",
                textDecoration: "none",
                fontSize: "1.75rem",
                fontWeight: "900",
                lineHeight: "1.1"
            },
        },
        '& .articleCardsDescription': {
            display: "flex",
            alignItems: "center",
            fontSize: "80%",
            marginBottom: "8px",
            '& div': {
                margin: "0 5px",
            },
            '& p': {
                margin: "0",
                fontFamily: "Merriweather,Georgia,serif",
            },
        },
        '& .articleCardsText': {
            margin: "0 1.75rem 0 0"
        },



    }
})