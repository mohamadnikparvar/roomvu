import { GlobalState } from '@/redux/store/reducer';
import { ArticleItemProps } from '@/redux/store/reducer/reducers';
import React from 'react';
import { useSelector } from 'react-redux';
import Navbar from '../common/navbar/Navbar';
import AboutMe from './AboutMe';
import ArticleCards from './ArticleCards';


const HomePage = () => {
    const { list } = useSelector((state: GlobalState) => state.articles)
    return (
        <div>
            <Navbar />
            <AboutMe />
            {list.map((item: ArticleItemProps) => {
                return (<ArticleCards key={item.id} props={item} />)
            })}
        </div>
    );
};

export default HomePage;