import Image from 'next/image';
import React from 'react';
import { StyledAboutMe } from './styles';

const AboutMe = () => {
    return (
        <StyledAboutMe>
            <aside>
                <div className='aboutMeContainer'>
                    <Image
                        src="/profile.jpeg"
                        width={56}
                        height={56}
                        alt="Picture of the author" />
                    <p className='aboutUsDescription'>
                        Personal blog by
                        <a href="https://mobile.twitter.com/dan_abramov">Dan Abramov</a> . <br />
                        I explain with words and codes .
                    </p>
                </div>
            </aside>
        </StyledAboutMe>
    );
};

export default AboutMe;