import Link from 'next/link';
import React from 'react';
import { StyledArticleCards } from './styles';

import { trunc } from '@/helpers';
import { ArticleItemProps } from '@/redux/store/reducer/reducers';

const ArticleCards: React.FC<{ props: ArticleItemProps }> = ({ props }) => {
    const year = 2000 + parseInt(String(props.id / 30))
    const month = props.id % 30 + 1
    return (
        <StyledArticleCards>
            <article>
                <header>
                    <h3 className='articleCardsTitle'>
                        <Link href={`/posts/${props.id}`}>{props.title}</Link>
                    </h3>
                    <div className='articleCardsDescription'>
                        <p> {year} / {month}</p>
                        <div>-</div>
                        <p>{props.userId} min read</p>
                    </div>
                </header>
                <p className='articleCardsText'>{trunc(props.body, 100)}</p>
            </article>
        </StyledArticleCards>
    );
};

export default ArticleCards;