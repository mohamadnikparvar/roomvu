import { Box, styled } from '@mui/material'
import { BoxProps } from '@mui/system'

export const StyledPostPage = styled(Box)<BoxProps>(({ }) => {
    return {
        '& a': {
            fontSize: "32px"
        },
    }
})