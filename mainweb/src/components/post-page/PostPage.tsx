import { ArticleItemProps } from '@/redux/store/reducer/reducers';
import React from 'react';
import { StyledPostPage } from './styles';

const PostPage: React.FC<{ props: ArticleItemProps }> = ({ props }) => {
    return (
        <StyledPostPage>
            <a>{props.title}</a>
            <p>publication date: {props.id} </p>
            <p>{props.body}</p>
        </StyledPostPage>
    );
};

export default PostPage;