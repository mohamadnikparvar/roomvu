import { compose } from 'redux'
import { createWrapper } from 'next-redux-wrapper'
import reducer from './reducer'
import { configureStore } from '@reduxjs/toolkit'
import thunk from 'redux-thunk'

let composeEnhancers = compose

// Check if function running on the sever or client
if (typeof window !== 'undefined') {
    // Setup Redux Debuger
    composeEnhancers =
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
}

const store = configureStore({
    reducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
});

export const makeStore = () => store

// export an assembled wrapper
export const wrapper = createWrapper(makeStore, { debug: false })
