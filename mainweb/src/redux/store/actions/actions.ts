/* eslint-disable camelcase */
import http from '@/api/http'
import {
    FETCH_ARTICLES, FETCH_POST,
} from './types'
import { DARK_MODE } from "./types";

// articles actions
export const fetchArticle =
    () =>
        async (dispatch: (arg0: { type: string; list: any; loading: boolean; }) => void) => {
            try {
                dispatch({
                    type: FETCH_ARTICLES,
                    list: [],
                    loading: true,
                })
                const { data } = await http.get('/posts')
                dispatch({
                    type: FETCH_ARTICLES,
                    list: data,
                    loading: true,
                })
            } catch (err) {
                dispatch({
                    type: FETCH_ARTICLES,
                    list: [],
                    loading: false,
                })

                // Handle Err
            }
        }

// post screen actions
export const fetchPost =
    (id: string) =>
        async (dispatch: (arg0: { type: string; data: any; loading: boolean; }) => void) => {

            try {
                dispatch({
                    type: FETCH_POST,
                    data: {},
                    loading: true,
                })
                const { data } = await http.get(`/posts/${id}`)

                dispatch({
                    type: FETCH_POST,
                    data: data,
                    loading: false,
                })

                // Handle Res
            } catch (err) {
                dispatch({
                    type: FETCH_POST,
                    data: {},
                    loading: false,
                })
                // Handle Err
            }
        }


// dark mode actions
export const handledarkMode = (e: string) => async (dispatch: any) => {
    // getting the true or false value from the parameter and saving that to localstorage
    localStorage.setItem("darkmode", e);

    //dispatch data to reducer to be accessed as payload.action
    dispatch({
        type: DARK_MODE,
        payload: e,
    });
};

