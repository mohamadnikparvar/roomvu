import { HYDRATE } from 'next-redux-wrapper'
import { AnyAction, combineReducers } from 'redux'
import { articles, ArticlesStateProps, darkMode, post, PostStateProps } from './reducers'

export interface GlobalState {
    articles: ArticlesStateProps
    post: PostStateProps
    darkMode: typeof darkMode
}

const combine = combineReducers({
    articles,
    post,
    darkMode,
})

const rootReducer = (state: any, action: AnyAction) => {
    if (action.type === HYDRATE) {
        const nextState = { ...state, ...action.payload }
        return nextState
    }
    return combine(state, action as any)
}

export default rootReducer
