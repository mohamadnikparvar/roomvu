import { DARK_MODE, FETCH_ARTICLES, FETCH_POST } from '../actions/types'

export interface ArticleItemProps {
    // ----
    userId: number,
    id: number,
    title: string,
    body: string

}
export interface ArticlesStateProps {
    list: ArticleItemProps[]
    loading: boolean
}

interface ArticleActionProps extends ArticlesStateProps {
    type: string
}
export interface PostStateProps {
    data?: ArticleItemProps
    loading: boolean
}
export interface PostActionProps extends PostStateProps {
    type: string
}

const darkModeInitialState = {
    // checking mode from localstorage if falsey (e.g. 0, null, undefined, etc.) it will be false, otherwise true
    isdarkMode: !JSON.parse(typeof window !== 'undefined' && localStorage.getItem("darkmode") || "{}"),
};

// articles reducer
export const articles = (
    state: ArticlesStateProps = { list: [], loading: false },
    { type, list, loading }: ArticleActionProps,
): ArticlesStateProps => {
    switch (type) {
        case FETCH_ARTICLES:
            return { list, loading }

        default:
            return state
    }
}

// post reducer
export const post = (
    state: PostStateProps = {
        data: {
            userId: 0,
            id: 0,
            title: "",
            body: ""
        }, loading: false
    },
    { type, data, loading }: PostActionProps,
): PostStateProps => {
    switch (type) {
        case FETCH_POST:
            return { data, loading }

        default:
            return state
    }
}

// dark mode reducer
export const darkMode = (state = darkModeInitialState, action: { type: any; payload: any; }) => {
    switch (action.type) {
        case DARK_MODE:

            return {
                ...state,
                // getting value from the action file and changing isdarkMode State.
                isdarkMode: action.payload,
            };
        default:

            return state;
    }
};

