import { wrapper } from '@/redux/store';
import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import React from 'react';
import { Provider } from 'react-redux';

const App = ({ Component, pageProps }: AppProps) => {
  const { store } = wrapper.useWrappedStore(pageProps);
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>);

};

export default App;

