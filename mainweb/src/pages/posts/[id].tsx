import Navbar from '@/components/common/navbar/Navbar';
import PostPage from '@/components/post-page/PostPage';
import { wrapper } from '@/redux/store';
import { fetchPost } from '@/redux/store/actions/actions';
import { GlobalState } from '@/redux/store/reducer';
import { Container } from '@mui/material';
import { NextPage } from 'next';
import React from 'react';
import { useSelector } from 'react-redux';

const Post: NextPage<any> = () => {
    const { data } = useSelector((state: GlobalState) => state.post)
    return (
        <Container>
            <Navbar />
            <PostPage props={data as any} />
        </Container>
    );
};
Post.getInitialProps = wrapper.getInitialPageProps(
    store =>
        async ({ query }) => {
            const { id } = query
            await store.dispatch(fetchPost(id as any))

            return {
                defaultQueries: { id },
            }
        },
)
export default Post;